/**
 * 
 */
package zkComponent.zkoss;

import java.util.List;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;

import zkComponent.domain.PhongTro;
import zkComponent.service.CRUDService;

/**
 * @author Giang
 *
 */
public class CRUDPhongTroVM {
	@WireVariable
	private CRUDService crudService;

	private String keyword;

	private String id_phongtro;
	private String ten_phongtro;

	private PhongTro newPT = new PhongTro();

	private PhongTro selectPt;

	private List<PhongTro> listPT = null;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getId_phongtro() {
		return id_phongtro;
	}

	public void setId_phongtro(String id_phongtro) {
		this.id_phongtro = id_phongtro;
	}

	public String getTen_phongtro() {
		return ten_phongtro;
	}

	public void setTen_phongtro(String ten_phongtro) {
		this.ten_phongtro = ten_phongtro;
	}

	public PhongTro getNewPT() {
		return newPT;
	}

	public void setNewPT(PhongTro newPT) {
		this.newPT = newPT;
	}

	public PhongTro getSelectPt() {
		return selectPt;
	}

	public void setSelectPt(PhongTro selectPt) {
		this.selectPt = selectPt;
	}

	public List<PhongTro> getListPT() {
		return listPT;
	}

	public void setListPT(List<PhongTro> listPT) {
		this.listPT = listPT;
	}

	@AfterCompose
	public void initSetup() {
		crudService = (CRUDService) SpringUtil.getBean("CRUDService");
		listPT = crudService.getAll(PhongTro.class);
	}

	@Command
	@NotifyChange("listPT")
	public void deletePT(@BindingParam("delPT") PhongTro delPT) {
		this.selectPt = delPT;
		crudService.delete(this.selectPt);
		listPT = crudService.getAll(PhongTro.class);
	}

	@Command
	@NotifyChange("listPT")
	public void editPT(@BindingParam("editPT") PhongTro editPT) {
		this.selectPt = editPT;
		System.out.println(selectPt.getTen());
		crudService.update(this.selectPt);
		listPT = crudService.getAll(PhongTro.class);
	}

	@Command
	@NotifyChange("listPT")
	public void AddPT() {
		System.out.println(Integer.parseInt(id_phongtro));
		System.out.println(ten_phongtro);

		newPT.setId(Integer.parseInt(id_phongtro));
		newPT.setTen(ten_phongtro);

		crudService.save(newPT);
		listPT = crudService.getAll(PhongTro.class);
	}

	@Command
	@NotifyChange("listPT")
	public void SearchPT() {
		listPT = crudService.searchPT(keyword);
	}
}
