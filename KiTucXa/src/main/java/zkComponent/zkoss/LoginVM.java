/**
 * 
 */
package zkComponent.zkoss;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import zkComponent.service.CRUDService;

/**
 * @author Giang
 *
 */
public class LoginVM {
	@WireVariable
	private CRUDService crudService;
	
	private String user;
	private String pass;
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	@Command
	public void submit(){
		
	}
}
