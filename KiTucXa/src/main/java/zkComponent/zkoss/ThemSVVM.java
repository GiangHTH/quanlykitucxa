/**
 * 
 */
package zkComponent.zkoss;

import java.util.List;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
/*import org.zkoss.bind.annotation.ExecutionArgParam;*/
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Window;

import zkComponent.domain.PhongTro;
import zkComponent.domain.SinhVien;
import zkComponent.domain.TaiKhoan;
import zkComponent.service.CRUDService;

/**
 * @author Giang
 *
 */
public class ThemSVVM {
	@WireVariable
	private CRUDService crudService;
	
	@Wire("#modalThemSV")
	private Window win;
	
	private SinhVien sv = new SinhVien();
	private PhongTro selectedPt = new PhongTro();
	private TaiKhoan tk = new TaiKhoan();
	
	private String id;
	private String ten;
	private String email;
	private String sdt;
	
	private List<PhongTro> phongtro = null;

	public SinhVien getSv() {
		return sv;
	}

	public void setSv(SinhVien sv) {
		this.sv = sv;
	}

	public PhongTro getSelectedPt() {
		return selectedPt;
	}

	public void setSelectedPt(PhongTro selectedPt) {
		this.selectedPt = selectedPt;
	}

	public TaiKhoan getTk() {
		return tk;
	}

	public void setTk(TaiKhoan tk) {
		this.tk = tk;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	public List<PhongTro> getPhongtro() {
		return phongtro;
	}

	public void setPhongtro(List<PhongTro> phongtro) {
		this.phongtro = phongtro;
	}
	
/*	private Object vmList;*/
	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view/*, @ExecutionArgParam("vmList") Object vmList*/){
		Selectors.wireComponents(view, this, false); 
		crudService = (CRUDService) SpringUtil.getBean("CRUDService");
		/*this.vmList = vmList;*/
		phongtro = crudService.getAll(PhongTro.class);
	}
	
	
	@Command
	public void SaveInfo(){
		tk.setUsername(id);
		
		sv.setId(id);
		sv.setTen(ten);
		sv.setEmail(email);
		sv.setSdt(sdt);
		sv.setPhongTro(selectedPt);
		sv.setTk(tk);
		tk.setSv(sv);
		selectedPt.getListSV().add(sv);
		
		crudService.save(sv);
		win.detach();
//		BindUtils.postNotifyChange(null, null, this.vmList, "listSV");
//		Executions.sendRedirect("admin_homePage.zul");
	}
	

	@Command
	public void CloseThis(){
		win.detach();
	}
}
