/**
 * 
 */
package zkComponent.zkoss;

import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Window;

import zkComponent.domain.PhongTro;
import zkComponent.domain.SinhVien;
import zkComponent.domain.TaiKhoan;
import zkComponent.service.CRUDService;

/**
 * @author Giang
 *
 */
public class SuaSVVM {
	@WireVariable
	private CRUDService crudService;
	
	/*@Autowired
	private SessionFactory sessionFactory;
	private Session session = sessionFactory.getCurrentSession();*/
	
	@Wire("#modalSuaSV")
	private Window win;
	
	private SinhVien sv = new SinhVien();
	private PhongTro selectedPt = new PhongTro();
	private TaiKhoan tk = new TaiKhoan();
	private List<PhongTro> phongtro = null;
	
	public SinhVien getSv() {
		return sv;
	}

	public void setSv(SinhVien sv) {
		this.sv = sv;
	}

	public PhongTro getSelectedPt() {
		return selectedPt;
	}

	public void setSelectedPt(PhongTro selectedPt) {
		this.selectedPt = selectedPt;
	}

	public TaiKhoan getTk() {
		return tk;
	}

	public void setTk(TaiKhoan tk) {
		this.tk = tk;
	}

	public List<PhongTro> getPhongtro() {
		return phongtro;
	}

	public void setPhongtro(List<PhongTro> phongtro) {
		this.phongtro = phongtro;
	}

	private Object vmList;
	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("editSV") SinhVien editSV, @ExecutionArgParam("vmList") Object vmList) {
		Selectors.wireComponents(view, this, false);
		crudService = (CRUDService) SpringUtil.getBean("CRUDService");
		this.sv = editSV;
		this.vmList = vmList;
		phongtro = crudService.getAll(PhongTro.class);
	}
	
	@Command
	public void EditInfo(){
		/*sv.setPhongTro(selectedPt);*/
		System.out.println(sv.getId());
		System.out.println(sv.getEmail());
		System.out.println(sv.getTen());
		System.out.println(sv.getSdt());
		System.out.println(sv.getPhongTro().getId());
		System.out.println(sv.getPhongTro().getTen());
		System.out.println(sv.getTk().getPassword());
		
		
		crudService.update(this.sv);
		BindUtils.postNotifyChange(null, null, this.vmList,"listSV");
		win.detach();
		//Executions.sendRedirect("admin_homePage.zul");
	}
	
	/*@Command
	public void CloseThis(){
		win.detach();
	}*/
}
