/**
 * 
 */
package zkComponent.zkoss;

import java.util.HashMap;
import java.util.List;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.SpringUtil;

import zkComponent.domain.SinhVien;
import zkComponent.service.CRUDService;

/**
 * @author Giang
 *
 */
public class CRUDSinhVienVM {
	@WireVariable
	private CRUDService crudService;

	private String keyword;
	
	private List<SinhVien> listSV = null;
	
	private SinhVien selectedSV;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public SinhVien getSelectedSV() {
		return selectedSV;
	}

	public void setSelectedSV(SinhVien selectedSV) {
		this.selectedSV = selectedSV;
	}

	public List<SinhVien> getListSV() {
		return listSV;
	}

	public void setListSV(List<SinhVien> listSV) {
		this.listSV = listSV;
	}
	
	@AfterCompose
	public void initSetup(){
		crudService = (CRUDService) SpringUtil.getBean("CRUDService");
		listSV = crudService.getAll(SinhVien.class);
	}
	
	@Command
	public void ShowNewSV(/*@BindingParam("vmList") Object vmList*/){
		/*HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("vmList", vmList);*/
		Executions.createComponents("admin_themSV.zul", null, null);
	}
	
	@Command
	@NotifyChange("listSV")
	public void deleteSV(@BindingParam("delSV") SinhVien delSV){
		this.selectedSV = delSV;
		crudService.delete(this.selectedSV);
		listSV = crudService.getAll(SinhVien.class);
	}
	
	@Command
	public void editSV(@BindingParam("editSV") SinhVien editSV, @BindingParam("vmList") Object vmList){
		this.selectedSV = editSV;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("editSV", this.selectedSV);
		map.put("vmList", vmList);
		Executions.createComponents("admin_suaSV.zul", null, map);
	}
	
	@Command
	@NotifyChange("listSV")
	public void SearchSV(){
		listSV = crudService.searchSV(keyword);
	}
}
