/**
 * 
 */
package zkComponent.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * @author Giang
 *
 */
@Entity
@Table(name = "taikhoan")
/*@NamedQuery(name = "taikhoan.findUserByUserID", query = "SELECT tk  FROM taikhoan  as tk WHERE tk.username = ?")*/
public class TaiKhoan implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private String password;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn
	private SinhVien sv;
	
	public TaiKhoan() {
	}
	
	public TaiKhoan(String username, String password, SinhVien sv) {
		this.username = username;
		this.password = password;
		this.sv = sv;
	}
	
	public TaiKhoan(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public SinhVien getSv() {
		return sv;
	}
	public void setSv(SinhVien sv) {
		this.sv = sv;
	}
	
	
}
