/**
 * 
 */
package zkComponent.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * @author Giang
 *
 */
@Entity
@Table(name = "sinhvien")
public class SinhVien implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "ten")
	private String ten;

	@Column(name = "email")
	private String email;

	@Column(name = "sdt")
	private String sdt;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "phongTro_id", nullable = true)
	private PhongTro phongTro;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER, mappedBy="sv")
	private TaiKhoan tk;
	
	public SinhVien() {
	}

	public SinhVien(String id, String ten, String email, String sdt, PhongTro phongTro, TaiKhoan tk) {
		this.id = id;
		this.ten = ten;
		this.email = email;
		this.sdt = sdt;
		this.phongTro = phongTro;
		this.tk = tk;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	public PhongTro getPhongTro() {
		return phongTro;
	}

	public void setPhongTro(PhongTro phongTro) {
		this.phongTro = phongTro;
	}

	public TaiKhoan getTk() {
		return tk;
	}

	public void setTk(TaiKhoan tk) {
		this.tk = tk;
	}

}
