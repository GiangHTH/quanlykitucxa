/**
 * 
 */
package zkComponent.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Giang
 *
 */
@Entity
@Table(name = "phongtro")
public class PhongTro implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", nullable = false)
	private int id;

	@Column(name = "ten")
	private String ten;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER, mappedBy="phongTro")
	private Set<SinhVien> listSV = new HashSet<SinhVien>();
	
	public PhongTro() {
	}

	public PhongTro(int id, String ten, Set<SinhVien> listSV) {
		this.id = id;
		this.ten = ten;
		this.listSV = listSV;
	}

	public PhongTro(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTen() {
		return ten;
	}

	public void setTen(String ten) {
		this.ten = ten;
	}

	public Set<SinhVien> getListSV() {
		return listSV;
	}

	public void setListSV(Set<SinhVien> listSV) {
		this.listSV = listSV;
	}

}
