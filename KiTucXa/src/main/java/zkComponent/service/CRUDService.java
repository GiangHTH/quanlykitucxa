/**
 * 
 */
package zkComponent.service;

import java.util.List;

import zkComponent.domain.PhongTro;
import zkComponent.domain.SinhVien;

/**
 * @author Giang
 *
 */
public interface CRUDService {
	<T> List<T> getAll(Class<T> klass);
	<T> void save(T klass);
	<T> void delete(T klass);
	<T> void update(T klass);
	<T> T GetUniqueEntityByNamedQuery(String query, Object... params);
	public List<SinhVien> searchSV(String keyword);
	public List<PhongTro> searchPT(String keyword);
}
