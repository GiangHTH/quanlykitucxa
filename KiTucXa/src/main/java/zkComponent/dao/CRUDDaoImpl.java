/**
 * 
 */
package zkComponent.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * @author Giang
 *
 */
@Repository
public class CRUDDaoImpl implements CRUDDao {

	@Autowired
	private SessionFactory sessionFactory;

	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see zkComponent.dao.CRUDDao#getAll(java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getAll(Class<T> klass) {
		return getCurrentSession().createQuery("from " + klass.getName()).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see zkComponent.dao.CRUDDao#save(java.lang.Object)
	 */
	@Override
	public <T> void save(T klass) {
		getCurrentSession().save(klass);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see zkComponent.dao.CRUDDao#delete(java.lang.Object)
	 */
	@Override
	public <T> void delete(T klass) {
		getCurrentSession().delete(klass);
	}

	@Override
	public <T> void update(T klass) {
		getCurrentSession().update(klass);

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T GetUniqueEntityByNamedQuery(String query, Object... params) {
		Query q = getCurrentSession().getNamedQuery(query);
		int i = 0;

		for (Object o : params) {
			q.setParameter(i, o);
		}

		List<T> results = q.list();

		T foundentity = null;
		if (!results.isEmpty()) {
			// ignores multiple results
			foundentity = results.get(0);
		}
		return foundentity;
	}

}
